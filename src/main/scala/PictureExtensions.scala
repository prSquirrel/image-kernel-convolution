/*
Algorithm derived from playing with:
http://setosa.io/ev/image-kernels/
*/

import java.awt.Color
import Picture._

import concurrent.Future
import concurrent.ExecutionContext.Implicits.global
import util.{Success, Failure}
import concurrent.duration._
import scala.concurrent.Await

object PictureExtensions {

  implicit class RichPicture(pic1: Picture) {
    val width = pic1.width
    val height = pic1.height
    //TODO:move all loop logic into convolute, so it operates on the whole Picture at once
    def convolute(pos: (Int, Int), kernel: Seq[Double]): Color = {
      //TODO: make this more generic, so it is possible to use not just 3x3, but any kernel dimensions
      val perms = for {
        x <- Seq(-1, 0, 1)
        y <- Seq(-1, 0, 1)
      } yield (x, y)

      val (i, j) = pos

      import ColorExtensions.ColorOps

      val cmb = (perms, kernel).zipped.map { (offset, factor) => {
        val neighbour = pic1.get(i + offset._1, j + offset._2)
        neighbour * factor
      }
      }

      //sum the products
      cmb.foldLeft(new Color(0, 0, 0))(_ + _)
    }
    
    def grayscale(): Picture = {
        val outputPic = new Picture(width, height)
        for(i <- 0 until width) {
            for(j <- 0 until height) {
                val c = pic1.get(i, j)
                val r = c.getRed
                val g = c.getGreen
                val b = c.getBlue
                val a: Double = (r + g + b) / 3.0
                val newColor = new Color(a.toInt, a.toInt, a.toInt) 
                outputPic.set(i, j, newColor)
            }
        }
        outputPic
    }
    def apply(kernel: Seq[Double], interactive: Boolean = false): Picture = {
      val width = pic1.width
      val height = pic1.height
      val outputPic = new Picture(pic1)//new Picture(width - 2, height - 2)
      //we omit edges of the image to more easily apply kernel

      //1 pass -> distribute the work over Futures
      //type: Seq[Future[Seq[Future[Unit]]]]
      val computation = for (i <- 1 until width - 1) yield Future {
        for (j <- 1 until height - 1) yield Future {
          outputPic.set(i, j, pic1.convolute((i, j), kernel))
          if(interactive) {outputPic.show()}
        }
      }
      //2 pass -> simplify
      //type: Seq[Future[Seq[Unit]]]
      val sequenced0 = computation.map { s =>
        s.flatMap { x =>
          Future.sequence(x)
        }
      }
      //3 pass -> simplify even more
      //type: Future[Seq[Seq[Unit]]]
      val sequenced = Future.sequence(sequenced0)//.map {x => x.flatten}

      //block until all futures have completed their work
      {
        Await.ready(sequenced, Duration.Inf)
        outputPic
      }
    }
  }

}