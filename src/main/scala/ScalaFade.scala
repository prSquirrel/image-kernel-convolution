//Directly translated from Java

import java.awt.Color
import Picture._

object ScalaFade {
  def combine(c1: Color, c2: Color, alpha: Double) = {
    val r = alpha * c1.getRed + (1 - alpha) * c2.getRed
    val g = alpha * c1.getGreen + (1 - alpha) * c2.getGreen
    val b = alpha * c1.getBlue + (1 - alpha) * c2.getBlue
    new Color(r.toInt, g.toInt, b.toInt)
  }

  def main(args: Array[String]): Unit = {
    val N = 500
    val pic1 = new Picture("image1.jpg")
    val pic2 = new Picture("image2.jpg")
    val width = pic1.width
    val height = pic1.height

    val pic = new Picture(width, height)
    for (n <- 0 to N) {

      val alpha = 1.0 * n / N
      for (i <- 0 until width) {
        for (j <- 0 until height) {
          val c1 = pic1.get(i, j)
          val c2 = pic2.get(i, j)
          pic.set(i, j, combine(c2, c1, alpha))
        }
      }
      pic.show()

    }
  }
}
