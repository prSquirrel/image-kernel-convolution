//TODO: implement divisor, like in GIMP
//TODO: implement normalization
object Filter extends Enumeration {
  implicit def toDouble(f: Seq[Int]): Seq[Double] = f.map(_.toDouble)
  def normalize(kernel: Seq[Double]): Seq[Double] = {
      val n = kernel.foldLeft(0.0)((s, e) => s + math.abs(e))
      //val n = kernel.sum
      if(n != 0.0) kernel.map(x => x/n) else kernel
  }
  private val b = 1.04
  val Blur = Seq(
      1,4,1,
      4,2,4,
      1,4,1
  ).map(x => x*b)
  val BlurNorm = Seq(
    b / 16, b / 8, b / 16,
    b / 8, b / 4, b / 8,
    b / 16, b / 8, b / 16
  )
  val BottomSobel = Seq(
    -1, -2, -1,
    0, 0, 0,
    1, 2, 1
  )
  val Emboss = Seq(
    -2, -1, 0,
    -1, 1, 1,
    0, 1, 2
  )
  val Identity = Seq(
    0, 0, 0,
    0, 1, 0,
    0, 0, 0
  )
  val LeftSobel = Seq(
    1, 0, -1,
    2, 0, -2,
    1, 0, -1
  )
  val EdgeDetection = Seq(
    1, 0, -1,
    0, 0, 0,
    -1, 0, 1
  )
  val Outline = Seq(
    8, -10, -6,
    -1, 20, -1,
    4, 7, 1
  )
  val RightSobel = Seq(
    -1, 0, 1,
    -2, 0, 2,
    -1, 0, 1
  )
  val Sharpen = Seq(
    0, -1.0/4, 0,
    -1.0/4, 2.0, -1.0/4,
    0, -1.0/4, 0
  )
  val Brighten = Seq(
      -1.0001,1.0001,-1.0001,
      1.0001,1,1.0001,
      -1.0001,1.0001,-1.0001
  )
  val TopSobel = Seq(
    1, 2, 1,
    0, 0, 0,
    -1, -2, -1
  )
  val Gradient = Seq(
    0, 0, 1.0,
    0, 1.0/3, 0,
    1.0, 0, 0
  )
}