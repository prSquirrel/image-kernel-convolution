import Picture._
import PictureExtensions.RichPicture
import Filter._

import concurrent.Future
import concurrent.ExecutionContext.Implicits.global

object ImageProcessing {
  def main(args: Array[String]): Unit = {
    val pic1 = new Picture("parrots.jpg")
    pic1.show()
    val newpic2 = pic1.grayscale.apply(Gradient)
    newpic2.show()
  }
}
