import java.awt.Color

object ColorExtensions {
  //Using Jesper's snippet
  //http://stackoverflow.com/questions/6199627/how-to-write-a-limit-function-in-scala
  @inline def clamp[@specialized(Int, Double) T: Ordering](value: T, low: T, high: T): T = {
    import Ordered._
    if (value < low) low else if (value > high) high else value
  }

  //wrapper to limit color values to the range 0...255
  def clampedColor(r: Int, g: Int, b: Int) = {
    new Color(clamp(r, 0, 255), clamp(g, 0, 255), clamp(b, 0, 255))
  }

  implicit class ColorOps(c1: Color) {
    def +(c2: Color) = {
      val r = c1.getRed + c2.getRed
      val g = c1.getGreen + c2.getGreen
      val b = c1.getBlue + c2.getBlue
      clampedColor(r, g, b)
    }

    def *(d: Double) = {
      val r = c1.getRed * d
      val g = c1.getGreen * d
      val b = c1.getBlue * d
      clampedColor(r.toInt, g.toInt, b.toInt)
    }

    def *(c2: Color) = {
      val r = c1.getRed * c2.getRed
      val g = c1.getGreen * c2.getGreen
      val b = c1.getBlue * c2.getBlue
      clampedColor(r, g, b)
    }
  }

}